# Functional Blocks schema

```mermaid
flowchart TB
    subgraph Federated Services
        subgraph Trust Framework Module
            subgraph Registry Service
                r1("Trust Anchor Service")
                r2("Federated Catalogue")
                r3("Schema Provider")
                r4("Authorization Registry")
                r5("Policy Rules Registry")
            end
            subgraph Compliance Service / Policy Rules Checker
                c1("Notarization Service")
                c2("Schema Validator")
                c4("Rules Checker")
                c3("VC Issuer")   
            end
        end
        subgraph Federation Web Portal
            subgraph "Federation Catalogue"
                r8("Common Federation Service Catalogue")
            end
        end
        r6("Service Description Wizard Tool")
        r7("DID Universal Resolver")
        r9("PDP (Policy Decision Point) <a href='https://csrc.nist.gov/glossary/term/policy_decision_point'>@</a> ")
        r10("OCM/PCM (Organization Credential Manager /  Personal Credential Manager) <a href='https://gitlab.com/gaia-x/data-infrastructure-federation-services/ocm'>@</a>")
        r11("User Agent")
    end
    
    subgraph "Legende"
        fonc("Fonctions")
        comp("Components")
    end

    style c1 fill:#87CEFA
    style c2 fill:#87CEFA
    style c3 fill:#87CEFA 
    style c4 fill:#87CEFA 
    style fonc fill:#87CEFA
```

Service Description Wizard tools


# Functional Blocks Explanations

## Trust Framework module

Ref Specification: Trust framework 22.04 https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.04/
Trust framework in TAD: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#gaia-x-trust-framework
Self-description Verification Process in TAD 5.6.2: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/self-description/
Self-description compliance in TAD 6.5.2: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#self-description-compliance

The Trust Framework module is the set of components in charge of establishing conformity and trust between actors inside a Federation. The different components of the Trust Framework module are operated by a Federator inside a Federation.

We distinguish two subset of components, the Compliance part and the Registry part.

### Compliance Service / Policy Rule Checker (PCR)

Existing component: Gaia-X Lab  https://gitlab.com/gaia-x/lab/compliance/gx-compliance

Gaia-X Compliance Service / PCR deliver a Gaia-x label to a Gaia-x Service Offering. 
The Gaia-x Compliance service / PCR perform checks : 
* syntactic correctness.
* schema validity.
* cryptographic signature validation.
* attribute value consistency.
* attribute value verification.

The Gaia-x Compliance Service / PCR uses an Label Rule Assignment component to get a label regarding claims provided. 
Gaia-X Label in TAD: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#gaia-x-labels
Gaia-x Label 

The Gaia-x Compliance service / PCR sends informations to the Notarization Service to generate a valid Verifiable Credential signed. 

#### Notarization Service

Existing component: https://gitlab.com/gaia-x/data-infrastructure-federation-services/not 

The Notarization service is in charge of signing Self Descriptions that will be trusted by the all actors of the federation. To achieve this, the Notarization will process Self Description creation requests, then it will compute a proof signed by the Federator keys and finally it will return the Self Description with its poof.

#### Label Rule Assignment

Rule engine computing Self Descriptions to assign a Gaia-x label according to policy rules.
Reference for the criteria: Gaia-X Labelling Criteria https://gaia-x.eu/wp-content/uploads/2022/05/Gaia-X-labelling-criteria-v22.04_Final.pdf

### VC Issuer

Tool to generate Service Descriptions from a simple catalog. 

### Schema Validator

Ensure a given Self Description body follows a well known schema defined in the Schema Provider. For example, a Self Description for a Participant must match the Participant JSON-LD or Participant SHACL file inside the Schema provider.

Current schemas can be found here : https://gaia-x.gitlab.io/technical-committee/service-characteristics/widoco/core/core.html

### Registry Service

Existing component: Gaia-X Lab https://gitlab.com/gaia-x/lab/compliance/gx-registry
reference in TAD: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#gaia-x-registry

The Gaia-X Registry is the backbone of the ecosystem governance which stores information. It is a public distributed, non-repudiable, immutable, permissionless database with a decentralized infrastructure and the capacity to automate code execution. (according to Architecture Document).

We are going to use this service to store all data we need to run a [Gaia-X ecosystem](https://gaia-x.gitlab.io/technical-committee/glossary/ecosystem/).

#### Trust Anchor Service

The Trust Anchor Service stores and exposes Trust Anchor entities. For a given ecosystem, the Trust anchors are the entities considered by all Participants to be trustworthy when establishing the chain of cryptographic certificates.

Registry API developed by Gaia-X Lab is able to manage Trust Anchor ([Registry API Swagger](https://registry.gaia-x.eu/docs/#/)).

Trust Anchor in TAD: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#trust-anchors 

#### Federated Catalogue

ref in TAD: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/federation_service/#federated-catalogue

Existing component: https://gitlab.com/gaia-x/data-infrastructure-federation-services/cat

Federated Catalogue stores service offering entities exposed by Participants Provider. With Federated Catalogue, Consumer is able to search the most appropriate service according to the searched labels.

#### Schema Provider

To validate request format, we need to check if the request is compliant with expected format which is a schema. Expected schema are stored in schema provider.

There is Gaia-X expected schema which are mandatories to be Gaia-X compliant.

#### Policy Rule Provider

Existing component (some part): https://gitlab.com/gaia-x/data-infrastructure-federation-services/tsa

reference: Policy Rules Document  https://gaia-x.eu/wp-content/uploads/2022/05/Gaia-X_Policy-Rules_Document_v22.04_Final.pdf

In addition to being in the correct format, requests must also validate functional rules. For example, If legalAddress.country is located in United States of America, than a valid legalAddress.state using the two-letter state abbreviations is mandatory.

This rules are stored inside Policy Rule Provider.

Some rules allow the assignment of a label. The rule provider is also responsible for returning the label associated with the rule if the rule is verified.

### Policy Decision Point

The Policy Decision Point represent the [PDP](https://csrc.nist.gov/glossary/term/policy_decision_point) (Policy Decision Point).

A **PDP** take three inputs :

```mermaid
flowchart TD

sr("Service policy rules")
br[("Business data referential")]
ua("User attributes\n(from VC)")
pdp{"PDP\n(policy engine)"}
d(["Decision\nauthorized or not"])
sr-->pdp
br-->pdp
ua-->pdp
pdp-->d
```

* **Service Policy Rules**: A set of rules, contextualized for the business scope of the service that determine why a user action is allowed or not. These rules are expressed in [Rego](https://www.openpolicyagent.org/docs/latest/policy-language/) of the [OPA](https://www.openpolicyagent.org) project. This set of rules can be retrieved from the authorization registry.
  * Exemple (in natural language): allow `user_id` to `create a volume` if `current usage` + `desired size of new volume` < `user quota`. 
* **Business data repository**: Is a local (service) data repository (typically a database) that contains per-user states for that service.
  * In our example: the `current usage` per `user_id`.
* **User attributes**: are extracted from the verifiable presentation at each request and contain information useful for making the authorization decision.
  * In our example: the `user quota` and the `user_id`.

In the context of Gaia-X the technology selected to manage PDP is [OPA/Rego](https://www.openpolicyagent.org).

The [Open Policy Agent] technology (https://www.openpolicyagent.org) can be used in two modes:

* **Daemon**: In a separate process that can act as a network proxy that evaluates the policy and relays the request if authorized.
* **Library**: Integrate OPA as a library into the backend service that evaluates the policy.

The original library supported by the project is available for Go, however there are a [multitude](https://www.openpolicyagent.org/docs/v0.15.1/ecosystem/#) of compatible environments and applications today.

The recommendation of the IAM team is to place the policy engine as close as possible to the business code, and therefore to favor the library approach. 

This avoids an additional transit of the request through the network but above all avoids having a service that accepts any request indiscriminately.

### Authorization Registry

A registry in which the authorization rules (Rego files) are stored in a versioned manner and under the exclusive control of a trusted authority.
It can be a simple Git repository.

## Federation Web Portal

A web portal to interact with federation services (can interact with User Agent API).

## User Agent

User Agent is the entry point for participant to interact with Federation services. His role is to orchestrate interactions between the different federation services.

Technically the User Agent can be an API or a SDK or a CLI.

## OCM (Organization Credential Manager)

Existing component: 
https://gitlab.com/gaia-x/data-infrastructure-federation-services/ocm

https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/federation_service/#identity-and-access-management

OCM allow to store and retrieve VP as provided by the supplier and signed by the supplier. 

## Utility componants

### DID Universal resolver

A DID universal resolver is available at : http://resolver.lab.gaia-x.eu:8080.  
See : https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/9 for more information.

### Service Description Wizard Tool

A tool to help filling in and managing Service Descriptions (with GUI)

# Components usage

## Step 01 - Onboard company B as a Provider

```mermaid
sequenceDiagram
    actor Bob
    participant User Agent
    Note right of User Agent: Authenticate and Authorize
    participant Schema Provider
    participant Schema Validator as [Compliance Service / PCR]<br>Schema Validator
    participant Notarization Service as [Compliance Service / PCR]<br>Notarization Service
    participant Policy Rule Checker as [Compliance Service / PCR]<br>Rule Checker
    participant Policy Rule Provider

    Bob->>+User Agent:Request Participant Schema
    User Agent->>+Schema Provider: Request Participant Schema
    Schema Provider-->>-User Agent: Participant schema
    User Agent-->>-Bob: Participant schema
    Bob->>Bob: Write a self signed SD 
    Bob->>+User Agent:Request Participant VC
    User Agent->>+Schema Validator: Validate Participant SD
    Schema Validator->>+Schema Provider: Request Participant Schema
    Schema Provider-->>-Schema Validator: Participant Schema
    Schema Validator-->>-User Agent: Participant Schema OK
    User Agent->>+Policy Rule Checker: Evaluate rules for Participant SD
    Policy Rule Checker->>+Policy Rule Provider: Request rules for Participant
    Policy Rule Provider-->>-Policy Rule Checker: Applicable Rules for a Participant SD
    Policy Rule Checker-->>-User Agent: Participant SD is OK according to rules
    User Agent->>+Notarization Service: Ask VC Participant genaration
    Notarization Service-->>-User Agent: VC Participant
    User Agent-->>-Bob: VC Participant
    Bob->>Bob: Self sign a VP based on the returned SD
    Bob->>Bob: Publish Participant VP
    Bob->>+User Agent:Validate Participant VP
    User Agent->>+Schema Validator: Request Participant VP Schema
    Schema Validator->>+Schema Provider: Request Participant VP Schema
    Schema Provider-->>-Schema Validator: Participant VP Schema
    Schema Validator-->>-User Agent: Ok
    User Agent->>+Notarization Service: Validate proof
    Notarization Service-->>-User Agent: Ok
    User Agent-->>-Bob: Ok
    Bob->>Bob: Store his VP in his OCM
```
## Step 02 - Add data storage service offering to the federated catalogue

```mermaid
sequenceDiagram
    actor Bob
    participant User Agent
    Note right of User Agent: Authenticate and Authorize
    participant Schema Provider
    participant Schema Validator as [Compliance Service / PCR]<br>Schema Validator
    participant Notarization Service as [Compliance Service / PCR]<br>Notarization Service
    participant Policy Rule Checker as [Compliance Service / PCR]<br>Policy Rule Checker
    participant Policy Rule Provider
    participant Federated Catalogue

    Bob->>+User Agent:Request Service Offering Schema
    User Agent->>+Schema Provider: Request Service Offering Schema
    Schema Provider-->>-User Agent: Service Offering schema
    User Agent-->>-Bob: Service Offering schema
    Bob->>Bob: Write a self signed SD 
    Bob->>+User Agent: Request Service Offering VC
    User Agent->>+Schema Validator: Validate Service Offering Schema
    Schema Validator->>+Schema Provider: Request Service Offering Schema
    Schema Provider-->>-Schema Validator: Service Offering Schema
    Schema Validator-->>-User Agent: Service Offering Schema is OK
    User Agent->>+Policy Rule Checker: Evaluate rules for Service Offering SD
    Policy Rule Checker->>+Policy Rule Provider: Request rules for Service Offering
    Policy Rule Provider-->>-Policy Rule Checker: Applicable Rules for a Service Offering SD
    Policy Rule Checker-->>-User Agent: Service Offering is OK according to rules
    User Agent->>+Notarization Service: Ask Service Offering VC generation
    Notarization Service-->>-User Agent: Service Offering VC
    User Agent-->>-Bob:Service Offering VC
    Bob->>Bob: Self sign a VP based on the returned VC
    Bob->>+User Agent:Validate Service Offering VP
    User Agent->>+Schema Validator: Request Service Offering VP Schema
    Schema Validator->>+Schema Provider: Request Service Offering VP Schema
    Schema Provider-->>-Schema Validator: Service Offering VP Schema
    Schema Validator-->>-User Agent: Ok
    User Agent->>+Notarization Service: Validate proof
    Notarization Service-->>-User Agent: Ok
    User Agent-->>-Bob: Ok
    Bob->>+Federated Catalogue: Write Service Offering
    note right of Federated Catalogue: Validate the VP
    Federated Catalogue-->>-Bob: Ok
    Bob->>Bob: Store his VP in his OCM
```

## Step 03 - Define GAIA-X level x label to the service

```mermaid
sequenceDiagram
    actor Bob
    participant User Agent
    Note right of User Agent: Authenticate and Authorize
    participant Schema Provider
    participant Schema Validator as [Compliance Service / PCR]<br>Schema Validator
    participant Notarization Service as [Compliance Service / PCR]<br>Notarization Service
    participant Label Rule Assignment as [Compliance Service / PCR]<br>Label Rule Assignment
    participant Policy Rule Checker as [Compliance Service / PCR]<br>Policy Rule Checker
    participant Federated Catalogue

    Bob->>Bob: Write a self signed VP with Service Offering SD as a claim<br>+ additional claims
    Bob->>+User Agent: Get GAIA-X Label for Service Offering
    User Agent->>+Schema Validator: Validate VP
    Schema Validator->>+Schema Provider: Request Service Offering Schema
    Schema Provider-->>-Schema Validator: Service Offering Schema
    note left of Schema Validator: Validate the VP
    Schema Validator-->>-User Agent: VP is OK
    User Agent->>+Label Rule Assignment: Get available GAIA-X label for Service Offering SD
    
    Label Rule Assignment->>+Policy Rule Checker: Validate claim
    Policy Rule Checker->>Policy Rule Checker: Check ComplianceReferenceCredential validity
    Policy Rule Checker-->>-Label Rule Assignment: OK
    Label Rule Assignment->>Label Rule Assignment: Give criteria VC to compliant service
    Label Rule Assignment->>Label Rule Assignment: Get associated labels to criteria
    
    Label Rule Assignment-->>-User Agent: GAIA-X label for the Service Offering
    User Agent->>+Notarization Service: Generate Label VC for Service Offering
    Notarization Service-->>-User Agent: Label VC for Service Offering
    User Agent-->>-Bob: Label VC for Service Offering
    note left of Bob: Create VP for the Service Offering + Label. Validate the VP
    Bob->>+Federated Catalogue: Write Label for Service Offering
    note right of Federated Catalogue: Validate the VP
    Federated Catalogue-->>-Bob: Ok
    Bob->>Bob: Store his VP in his Wallet
```

## Step 04 - Onboard company A as a consumer, create VC for Alice
```mermaid
%%Step 04 - Onboard company A as a consumer, create VC for Alice%%
sequenceDiagram
    actor marie as Marie <br/> <<actor admin>>
    actor alice as Alice <br/> <<actor>>
    participant fa as User Agent
    participant sp as Schema Provider
    
    participant sv as [Trust Framework Module] <br/> Schema Validator
    participant ns as [Trust Framework Module] <br/> Notarization Service
    participant prc as [Trust Framework Module] <br/> Policy Rule Checker
    note over sv,prc: Compliance Service / PCR

    participant prp as [Trust Framework Module] <br/> Policy Rule Provider
    participant fc as Federated Catalogue

    Note right of fa: Authenticate and Authorize

    alice->>+fa:Request Consumer Schema
    fa->>+sp: Request Schema
    sp-->>-fa: Service Offering Schema
    fa-->>-alice:Service Offering Schema

    alice->>alice: Write a consumer object
    
    rect rgb(255, 255, 100)
        note over marie,fc: See with Bastien
        
        alice->>+fa: Validate Service Offering
        fa->>+sv: Request Service Offering VP Schema
        sv->>+sp: Request SO VP Schema
        sp-->>-sv: SO VP Schema
        sv-->>-fa: OK
        fa-->>-alice: OK

        alice->>+fc: Write Service Offering
        note right of fc: Validate the VP
        fc-->>-alice: OK
    end
```


## Step 05 - Search services with labels

```mermaid
%%Step 05 - Search services with labels%%
sequenceDiagram
    actor alice as Alice <br/> <<actor>>
    participant fa as User Agent
    participant sp as Schema Provider
    
    participant sv as [Trust Framework Module] <br/> Schema Validator
    participant prc as [Trust Framework Module] <br/> Policy Rule Checker
    note over sv,prc: Compliance Service / PCR

    participant prp as [Trust Framework Module] <br/> Policy Rule Provider
    participant fc as Federated Catalogue

    alice->>+fa: Ask search rules
    fa->>+prp: Ask search rules
    prp-->>-fa: search rules set
    fa-->>-alice: search rules set

    alice->>alice: Create search request
    
    alice->>+fa: Search
    fa->>+prc: Search
    prc->>+prp: Ask search request rules
    prp-->>-prc: search rules set
    prc-->>-fa: request is OK
    fa->>+fc: Ask the whole catalogue
    fc-->>-fa: the whole catalogue
    fa-->+prc: Ask to apply search rules on catalogue
    prc-->>-fa: result
    fa-->-alice: result
```

## Step 06 - Consume a service
```mermaid
%%Step 06 - Consume a service%%
sequenceDiagram
    actor alice as Alice <br/> <<actor>>
    participant pa as Participant Agent
    participant si as Service Instance
    
    alice->>+pa: Authenticates
    pa-->-alice: OK
    
    alice->>+pa: Get service instances
    pa-->-alice: service instances

    alice->>+pa: Search
    pa-->-alice: result

    alice->>+si: Authenticates with VP to consume
    si-->-alice: authorize and consume
```
